import os
import glob
from conans import ConanFile, CMake, tools


class DemolibConan(ConanFile):
    name = "demolib"
    version = "0.0.11"
    license = "LINCENSE"
    author = "Dacian Herbei dacian@lusee.ch"
    url = "https://gitlab.com/kframe/demolib"
    description = "Demo library that provides the skeleton for a library using conan"
    topics = ("build", "demo library", "conan")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake", "cmake_find_package"
    requires = "gtest/1.10.0@kframe+lusee-package-registry/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        self.run("git clone https://gitlab.com/kframe/demolib")
        os.chdir("demolib")
        self.run("git checkout release-" + self.version)
        os.chdir("..")

    def build(self):
        cmake = self._configure_cmake()
        cmake.configure(source_folder="demolib")
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        self.copy("*.h", dst="include", src="demolib/include")
        self.copy("*demolib.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["demolib"]
        # Ordered list of include paths
        self.cpp_info.includedirs = ['include']
        # Directories where libraries can be found
        self.cpp_info.libdirs = ['lib']

    def _configure_cmake(self):
        cmake = CMake(self)
        return cmake
