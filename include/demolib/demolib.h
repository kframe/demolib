#pragma once

#ifdef WIN32
  #define HELLO_EXPORT __declspec(dllexport) 
#else
  #define HELLO_EXPORT  
#endif

#include <string>

namespace demolib {
  HELLO_EXPORT std::string greeting();
}
