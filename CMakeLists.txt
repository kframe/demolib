cmake_minimum_required(VERSION 3.10)
include(cmake/TagVersion.cmake)
createTagVersion()

project(demolib VERSION     ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}.${VERSION_TWEAK}
              DESCRIPTION "demolib is a demo library to show how to use conan as build system"
              LANGUAGES   CXX)

message(STATUS "Building - Project: ${PROJECT_NAME} - Version: ${CMAKE_PROJECT_VERSION}")

include(cmake/StandardProjectSettings.cmake)
include(cmake/Sanitizers.cmake)
include(cmake/CompilerWarnings.cmake)
include(cmake/IPO.cmake)
include(cmake/ClangFormat.cmake)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

# For documentation https://docs.conan.io/en/latest/reference/generators/cmake.html

conan_basic_setup(TARGETS         # Allow to target_link packets as CONAN_PKG::packet
                  NO_OUTPUT_DIRS  # Do not adjust the build output directories.
                  )

# set a variable to contain the string path for headers
SET(DEMO_LIB_HEADER
    include/demolib/demolib.h
)

# set a variable to contain the string path for sources
SET(DEMO_LIB_SRC
    src/demolib.cpp
)

# build the library with the given spurces
ADD_LIBRARY(demolib ${DEMO_LIB_HEADER} ${DEMO_LIB_SRC})

# Set the include directory path for both during building and when using the installed library.
target_include_directories(demolib PUBLIC
    $<BUILD_INTERFACE:${demolib_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

# create a library alias with synsense namespace
add_library(lusee::demolib ALIAS demolib)

# Set the c++ standard / compile-time options requested
target_compile_features(demolib PUBLIC cxx_std_20)

# compile with -fPIC
option(DEMOLIB_ENABLE_PIC "Enable position-independent code (-fPIC) for svejs" TRUE)

set_property(TARGET demolib PROPERTY POSITION_INDEPENDENT_CODE ${DEMOLIB_ENABLE_PIC})

# inter-procedural optimization option if supported by compiler (aka LTO).
enable_ipo(demolib)

# standard compiler warnings
set_project_warnings(demolib PRIVATE)

# sanitizer options if supported by compiler
enable_sanitizers(demolib)

# enable the creation of a clang-format target
target_clangformat_setup(demolib)

# This is needed to compile with pybind11 + C++17 with LLVM.
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  target_compile_options(demolib INTERFACE -fsized-deallocation)
endif()

enable_testing()
add_subdirectory(test)

include(cmake/StandardProjectExport.cmake)
# standard_project_export(demolib ${demolib_SOURCE_DIR}/cmake/demolib/demolibConfig.cmake)
