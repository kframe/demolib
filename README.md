# DemoLib

Demo library used with DemoApp project in order to show how to use conan in conjunction with a custom library.


instructions to create demolib with conan recipe:
conan new <name of the library>/0.0.1 -t

<name of the library> is the name of the library, demolib in this case.

Example: demolib/0.0.1 -t

conan create . <user>+<registry package project>/<channel>
<user> - user authorized on the project to create packages
<registry package project> - project containing all the registered connan packages
<channel> - this could be beta, demo or production or wherever this package is meant to be used. There is no restriction to the channel name

Example: conan create . kframe+lusee-package-registry/demo