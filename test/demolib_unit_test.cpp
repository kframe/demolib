#include "demolib/demolib.h"
#include <gtest/gtest.h>

TEST(CheckGreeting, PassingTest) { 
    EXPECT_EQ ("Hello World Release!", demolib::greeting());
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}