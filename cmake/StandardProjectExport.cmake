function(standard_project_export project_name project_cmake_configuration_file)

    install(TARGETS ${project_name}
            EXPORT ${project_name}Targets
            LIBRARY DESTINATION lib
            ARCHIVE DESTINATION lib
            RUNTIME DESTINATION bin
            INCLUDES DESTINATION include)

    install(EXPORT ${project_name}Targets DESTINATION lib/cmake/${project_name} FILE ${project_name}Targets.cmake NAMESPACE lusee::)

    install(DIRECTORY include/ DESTINATION include)

    include(CMakePackageConfigHelpers)

    write_basic_package_version_file(${project_name}ConfigVersion.cmake COMPATIBILITY SameMajorVersion)

    install(FILES ${project_cmake_configuration_file}
                  ${CMAKE_CURRENT_BINARY_DIR}/${project_name}ConfigVersion.cmake
            DESTINATION lib/cmake/${project_name})

endfunction()