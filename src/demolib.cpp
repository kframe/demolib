#include <iostream>

#include "demolib/demolib.h"

namespace demolib
{
    std::string greeting()
    {
#ifdef NDEBUG
        return "Hello World Release!";
#else
        return "Hello World Debug!";
#endif
    }
}